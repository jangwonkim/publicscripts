##
## Written by Jangwon Kim
## SAIL, USC
## jangwon@usc.edu
##
## This code read each wav file (noise-cancelled)
##   and mute the initial X sec. (to remove initial tic! sound)
##
## Usage: python mute_init_tic.py wav_renamed wav 0.5
##   input 1: directory path for input wav files
##   input 2: directory path for output wav files
##   input 3: initial X second to be muted

import glob
import os, sys
import matplotlib.pyplot as plt
from scipy.io import wavfile
import numpy as np

def initial_mute(data,num_frame_mute):
  data_out = np.empty_like(data)
  data_out[:] = data
  data_out[0:(num_frame_mute-1)] = 0
  return data_out

def main(argv):

  dir_in   = sys.argv[1]
  dir_out  = sys.argv[2]
  mute_sec = sys.argv[3]
  
  # Create output direcotry
  if not os.path.exists(dir_out):
    os.mkdir(dir_out)
  
  # Read each wav file from "dir_in"
  f_pattern = "".join((dir_in,'/*.wav'))
  for f in glob.glob(f_pattern):
  
    # basename
    f_b = os.path.basename(f)
    print "Processing " + f_b + "..."
  
    # Read each wav file from "dir_in"
    f_in_path = "".join((dir_in,'/',f_b))
    fs, sig = wavfile.read(f_in_path)
  
    # compute # frames to be muted  
    num_frame_mute = round(fs * float(mute_sec))
  
    # mute the intial part
    sig_out = initial_mute(sig,num_frame_mute)
    
    ### Plot before/after mute-process
    ##f, axarr = plt.subplots(2, sharex=True)
    ##axarr[0].plot(sig)
    ##axarr[1].plot(sig_out)
    ##plt.show()
    
    # save the output
    f_path = "".join((dir_out,'/',f_b))
    f_out = wavfile.write(f_path,fs,sig_out)
  
  print "DONE..."

if __name__ == "__main__":
    main(sys.argv)


