function f_gen_trans_from_forced_wrd_forced_phn_list_sent(list_sent_file, wrd_level_forced_align_file, phn_level_forced_align_file,trans_file_name)

sent_list = textread(list_sent_file, '%s','delimiter','\n');

[st, et, wrd] = textread(wrd_level_forced_align_file,'%f %f %s');

for which_wrd = size(wrd,1):(-1):1
    if (strcmp(wrd{which_wrd,1},'sp') == 1) || ...
        (strcmp(wrd{which_wrd,1},'sil') == 1)
        st(which_wrd) = []; et(which_wrd) = []; wrd(which_wrd,:) = [];
    end
end

space_ind = strfind(sent_list,' ');
num_wrd_each_sent = zeros(size(space_ind,1),1);
for which_sent = 1:size(space_ind,1)
    if isempty(sent_list{which_sent,1}) == 0
        num_wrd_each_sent(which_sent) = length(space_ind{which_sent,1}) + 1;
    end
end

end_wrd_ind_each_sent = cumsum(num_wrd_each_sent);

% double check
if end_wrd_ind_each_sent(end) ~= size(wrd,1), error('num wrd not match'); end;

[st_phn, et_phn, phn] = textread(phn_level_forced_align_file,'%f %f %s');

% clean up the phone level forced alignment file by ...
% remove 0 time phone (mostly sp)
for which_phn = length(st_phn):-1:1
    if (et_phn(which_phn) - st_phn(which_phn)) < 0.0000001
        st_phn(which_phn) = [];
        et_phn(which_phn) = [];
        phn(which_phn,:) = [];
    end
end

% print out trans file
fw = fopen(trans_file_name,'wt');
for which_wrd = 1:size(wrd,1)
    
    wrd_s = wrd{which_wrd,1};

    % find the index of the final word of each sentence
    sent_s_ind = min(find(end_wrd_ind_each_sent >= which_wrd));
    sent_s = sent_list{sent_s_ind,1};

    % find phone of each word
    [dum_min, phn_st_ind] = min(abs(repmat(st(which_wrd),length(st_phn),1) - st_phn));
    [dum_min, phn_et_ind] = min(abs(repmat(et(which_wrd),length(et_phn),1) - et_phn));
    
    % print out for each phone
    for which_phn = phn_st_ind(1):phn_et_ind(end)
        st_s = sprintf('%.2f',st_phn(which_phn));
        et_s = sprintf('%.2f',et_phn(which_phn));
        phn_s = phn{which_phn};
        % if current phone is (silence or short pause) and the last phone
        if (sum(strfind(phn_s,'sp') == 1) || sum(strfind(phn_s,'sil') == 1)) && ...
            (which_phn == phn_et_ind(end)) && (end_wrd_ind_each_sent(sent_s_ind) ~= which_wrd)
            print_s = [st_s ',' et_s ',' phn_s ',,' sent_s];
        elseif (sum(strfind(phn_s,'sp') == 1) || sum(strfind(phn_s,'sil') == 1)) && ...
            (which_phn == phn_et_ind(end)) && (end_wrd_ind_each_sent(sent_s_ind) == which_wrd)
            print_s = [st_s ',' et_s ',sil,,'];
        else
            print_s = [st_s ',' et_s ',' phn_s ',' wrd_s ',' sent_s];
        end
        fprintf(fw, '%s\n', print_s);
    end
    
end
fclose(fw);

% fill out sil between sentence, at the beginning, and at the end

system(['cp ' trans_file_name ' ./tmp']);
[st_s,et_s,phn,wrd,sent] = textread('tmp','%s %s %s %s %s','delimiter',',');

fw=fopen(trans_file_name,'wt');

% put initial silence
if ((strcmp(phn{1},'sil') == 0)) && (str2num(st_s{1})>0)
    st_ss = sprintf('%f',0);
    et_ss = st_s{1};
    print_s = [st_ss ',' et_ss ',sil,,'];
    fprintf(fw,'%s\n',print_s);    
end

for which_line = 1:(size(st_s,1)-1)

    print_s = [st_s{which_line} ',' et_s{which_line} ',' phn{which_line} ',' wrd{which_line} ',' sent{which_line}];
    fprintf(fw,'%s\n',print_s);
    
    % if there is no 'sil' between sentence
    if (strcmp(sent{which_line,1},sent{which_line+1,1}) == 0)
        if ((strcmp(phn{which_line+1},'sil') == 0)) && ((strcmp(phn{which_line},'sil') == 0))
            % print this 'sil' after above
            st_ss = et_s{which_line};
            et_ss = st_s{which_line+1};
            print_s = [st_ss ',' et_ss ',sil,,'];
            fprintf(fw,'%s\n',print_s);
        end
    end
end

% print out the last line
which_line = size(st_s,1);
print_s = [st_s{which_line} ',' et_s{which_line} ',' phn{which_line} ',' wrd{which_line} ',' sent{which_line}];
fprintf(fw,'%s\n',print_s);

% check the total length of all~.wav file and put silence between the last
% wrd and the end of file
[st_phnalign, et_phnalign, phn_phnalign] = textread(phn_level_forced_align_file,'%f %f %s');
if (strcmp(phn{which_line},'sil') == 0) && (strcmp(phn_phnalign{end},'sil') == 1) && (et_phnalign(end)-st_phnalign(end) >= 0.005)
    print_s = [et_s{which_line} ',' num2str(et_phnalign(end)) ',sil,,'];
    fprintf(fw,'%s\n',print_s);
end

fclose(fw);
system('rm ./tmp');



