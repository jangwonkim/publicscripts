%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script is written by Jangwon Kim
% Sep. 27 2015
% SAIL, USC
% any question: jangwon@usc.edu
% 
% It assumes that you already generated sail_align outputs from a manually
% corrected transcription
% 
% This code generate a trans file from forced alignment output
%   (*.forced.wrd and *.forced.phn)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
clc;

list_sent_file = '/home/jangwon/workspace/data/rtmri_timit/m3/M3_list_sent_for_sailalign.txt';
wrd_level_forced_align_file = '/home/jangwon/workspace/data/rtmri_timit/m3/alignment/allsent_16kHz.forced.wrd';
phn_level_forced_align_file = '/home/jangwon/workspace/data/rtmri_timit/m3/alignment/allsent_16kHz.forced.phn';
trans_file_name = '/home/jangwon/workspace/data/rtmri_timit/m3/allsent_16kHz.trans';

%% generate a trans file (wrd, phn, and sentence) for concatenated wav
f_gen_trans_from_forced_wrd_forced_phn_list_sent(list_sent_file, wrd_level_forced_align_file, phn_level_forced_align_file,trans_file_name);

