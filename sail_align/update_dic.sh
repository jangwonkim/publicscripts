#!/bin/sh
# This code is written by Jangwon Kim
# SAIL, USC
# email to jangwon@sail.usc.edu for any question
#
# Usage: bash update_dic.sh proc_key_key
#  e.g. 
#    bash update_dic.sh 1       for checking missing words
#    bash update_dic.sh 2       for checking if only allowed phones are used or not.
#    bash update_dic.sh 3       for creating new dictionary
#
# Detailed explanation how to use this script ====
# First, open update_dic.sh and change the paths of script (for sail align), dic (original dictionary) and newdic (output: new dictionary for the words in sailalign script).
# Then, type below in the terminal
# bash update_dic.sh 1
# This proc_key will find the "word + phone sequences" in the original dictionary for each word in the sail_align_script, and print out on tmp.dic_log
# 
# Open tmp.dic_log and check if there is any word without phone sequences
# If there is any, fill out the phone sequences for the words, just like the other words, then save.
# 
# Then type below
# bash update_dic.sh 2
# This proc_key will check if any not-allowed phone was used in tmp.dic_log
# The list of "Not-used phones" is OK. They are just the ones not used for the sail_align_script
# The list of "Extra phones (ERROR)" is NOT OK. If there is any, please open tmp.dic_log and change the phone to the right one (one in the phones.list)
# 
# Finally, type below
# bash update_dic.sh 3
# This proc_key will create a new dictionary for the words in sail align script.
# =================================================

# PARAMETERS
proc_key=$1
script='sail_align_script'
dic='"PATH_TO_SAILALIGN"/language/timit_dictionary.dic'
newdic='"PATH_TO_SAILALIGN"/language/usc_timit_m3.dic'
phnlist='phones.list_cmu_timit'

###################################
# Do not change from here
###################################

if [ $proc_key == 1 ]
then 
  # check missing words of your script in the original dictionary (e.g., timit_dictionary.dic)
  for eachwrd in `cat $script | tr " " "\n" | sort | uniq`
  do
    echo "---IF THE PHONES OF THIS WORD ARE MISSING OR MULTIPLE, FIX THEM MANUALLY--- $eachwrd";
    #grep "^$eachwrd	" -r /home/jangwon/speech_text_alignment/SailAlign-1.10/language/cmu_dictionary.dic;
    grep "^$eachwrd " -r $dic;    # When timit_dictionary.dic is used
    #grep "^$eachwrd	" -r $dic;    # When cmu_dictionary.dic is used
  done > tmp.dic_log
fi

if [ $proc_key == 2 ]
then
  cat tmp.dic_log | grep -v "IF THE PHONES OF THIS WORD ARE MISSING OR MULTIPLE, FIX THEM MANUALLY" | cut -f2- -d" " | tr " " "\n" | sort | uniq > tmp.newdic_phn
  cat $phnlist | grep -v 'sp\|sil' > tmp.dic_phn
  diff tmp.newdic_phn tmp.dic_phn > tmp.diff_phn
  phn_extra=`cat tmp.diff_phn | grep "<" | sed "s/<//g"`
  phn_missing=`cat tmp.diff_phn | grep ">" | sed "s/>//g"`
  echo "Not-used phones      : " $phn_missing
  echo "Extra phones (ERROR) : " $phn_extra
  rm tmp.dic_phn tmp.diff_phn tmp.newdic_phn
fi



# After manuall add phone sequences for the missing words manually
# then perform below.
if [ $proc_key == 3 ]
then
  grep -v 'IF THE PHONES OF THIS WORD ARE MISSING OR MULTIPLE, FIX THEM MANUALLY' tmp.dic_log > $newdic
fi
